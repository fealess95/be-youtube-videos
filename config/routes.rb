Rails.application.routes.draw do
  post '/login', to: "sessions#login"

  resources :shared_videos
end
